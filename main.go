package main

import (
	"fmt"
	"net/http"

	"github.com/esayemm/go-starter/config"
)

func main() {
	cfg, _ := config.New()

	fmt.Printf("%+v", cfg)

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("hello"))
	})

	if err := http.ListenAndServe(":"+cfg.Port, nil); err != nil {
		panic(err)
	}
}
