package config

import (
	"github.com/spf13/viper"
)

// Config contains app cfg variables
type Config struct {
	Port string
}

// New returns Config
func New() (*Config, error) {
	viper.SetDefault("PORT", "9090")

	viper.AutomaticEnv()

	cfg := new(Config)
	if err := viper.Unmarshal(cfg); err != nil {
		return nil, err
	}

	return cfg, nil
}
